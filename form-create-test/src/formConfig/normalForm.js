// 正常表单
// import {handleSingle} from './handle/handle'

let rule = [
  {
    type: 'input',
    title: '活动名称',
    field: 'active_name',
    value: ''
  },
  {
    type: 'input',
    title: '活动区域',
    field: 'active_zone',
    value: ''
  },
  {
    type: 'DatePicker',
    title: '活动时间',
    field: 'active_time',
    value: ''
  },
  {
    type: 'switch',
    title: '即时配送',
    field: 'distribution_now',
    value: false
  },
  {
    type: 'checkbox',
    title: '活动性质',
    field: 'active_type',
    value: [],
    options: [
      {
        value: 0,
        label: '美食/餐厅线上活动'
      },
      {
        value: 1, label: '地推活动'
      },
      {
        value: 2, label: '线下主题活动'
      },
      {
        value: 3, label: '单纯品牌曝光'
      }
    ]
  },
  {
    type: 'radio',
    title: '特殊资源',
    field: 'sp_res',
    value: '',
    options: [
      {value: 1, label: '线上品牌赞助'},
      {value: 2, label: '线下场地免费'}
    ]
  },
  {
    type: 'select',
    title: '代理人',
    field: 'agents',
    value: [],
    props: {
      multiple: true,
      filterable: true,
      'multiple-limit': 2
    },
    request: true,
    url: ''
  },
  {
    type: 'textarea',
    title: '活动形式',
    field: 'active_sy',
    value: ''
  }
]

const content = {
  rule
}

export default content
