const FormType = {
  /**
   * 经典表单
   */
  NORMAL: 1001,
  /**
   * 行内表单
   */
  INLINE: 1002,
  /**
   * 对齐方式
   */
  ALIGN: 1003,
  /**
   * 表单验证
   */
  VALIDATE: 1004,
  /**
   * 动态增减表单
   */
  DYNAMIC: 1005
}

export default FormType
