import FormType from './formType'
import {handleLayout} from './handle/handle'
import normalForm from './normalForm'

const config = new Map([
  [FormType.NORMAL, {obj: normalForm, name: '典型表单'}]
])

const layout = {labelWidth: '120px', span: 24};
[...config.values()].forEach(item => {
  if (!item.isLayout) {
    handleLayout(item.obj, layout)
  }
})

export default config
