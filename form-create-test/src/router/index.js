import Vue from 'vue'
import Router from 'vue-router'
import FormCreate from '../components/FormCreate.vue'
import Form1 from '../components/Form1'
import Form1Maker from '../components/Form1Maker'
import FormInline from '../components/FormInline'
import FormToolTip from '../components/FormToolTip'
import FormUpload from '../components/FormUpload'
import FormToolTipWithoutTmp from '../components/FormToolTipWithoutTmp'
import FormRender from '../components/FormRender'
import FormValueUpdate from '../components/FormValueUpdate'
import I18nDemo from '../components/I18nDemo'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/1',
      name: 'FormCreate',
      component: FormCreate
    },
    {
      path: '/2',
      name: 'Form1',
      component: Form1
    },
    {
      path: '/3',
      name: 'Form1Maker',
      component: Form1Maker
    },
    {
      path: '/4',
      name: 'FormInline',
      component: FormInline
    },
    {
      path: '/5',
      name: 'FormToolTip',
      component: FormToolTip
    },
    {
      path: '/6',
      name: 'FormUpload',
      component: FormUpload
    },
    {
      path: '/7',
      name: 'FormToolTipWithoutTmp',
      component: FormToolTipWithoutTmp
    },
    {
      path: '/8',
      name: 'FormRender',
      component: FormRender
    },
    {
      path: '/9',
      name: 'I18nDemo',
      component: I18nDemo
    },
    {
      path: '/',
      name: 'FormValueUpdate',
      component: FormValueUpdate
    }
  ]
})
