import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
const cn = {
  message: {
    label: '号码',
    text: '获取验证码',
    send: '已发送'
  },
  ...zhLocale
}

export default cn
