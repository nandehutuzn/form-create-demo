import enLocale from 'element-ui/lib/locale/lang/en'

const en = {
  message: {
    label: 'Phone',
    text: 'Get Verification',
    send: 'Sended'
  },
  ...enLocale
}

export default en
