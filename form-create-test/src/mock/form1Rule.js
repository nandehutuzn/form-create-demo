const mock = () => {
  return [
    {
      type: 'input',
      title: '商品名称',
      field: 'goods_name',
      value: '',
      props: {
        'type': 'text',
        'clearable': false,
        'disabled': false,
        'readonly': false,
        'rows': 4
      },
      validate: [
        {
          required: true,
          message: '请输入商品名称',
          trigger: 'blur'
        }
      ]
    },
    {
      type: 'radio',
      title: '是否包邮',
      field: 'is_postage',
      value: 0,
      options: [
        {
          value: 0, label: '不包邮', disabled: false
        },
        {
          value: 1, label: '包邮', disabled: false
        },
        {
          value: 1, label: '未知', disabled: true
        }
      ]
    }
  ]
}

export default mock
